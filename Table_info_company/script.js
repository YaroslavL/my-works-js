/*

  Данные: http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
  Задача.

  1.  Получить данные и в виде простой таблички вывести список компаний. Для начала используем поля:
      Company | Balance | Registered | Показать адресс | Кол-во employers | показать сотрудников

  2.  Сделать сортировку таблицы по количеству сотрудников и балансу. Сортировка должна происходить по клику
      на заголовок столбца

  3.  По клику на показать адресс должна собиратся строка из полей адресса и показываться на экран.

  4.  По клику на показать сотрудников должна показываться другая табличка формата:

      <- Назад к списку компаний | *Название компании*
      Сотрудники:
      Name | Gender | Age | Contacts

  5.  В второй табличке долен быть реализован поиск сотрудников по их имени, а так же сортировка по
      полу и возрасту.

  Примечание: Весь код должен писатся с учетом синтаксиса и возмжность ES6.

*/
//****************Получение данных*********************//
let data = fetch('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2')
  .then(response => response.json())
  .then(ConstructTable);


function ConstructTable(data) {
  //*******************Создание основной таблицы с первой строчкой (шапка таблицы)********//
  let div = document.getElementById('createTable');
  let table = document.createElement('table');
  table.setAttribute('id', 'table');
  table.classList.add('main_table');
  table.setAttribute('border', '1px groove blue');
  div.appendChild(table);

  let createFirstTr = `
    <tr>
      <td id="companySort">
        Company
      </td>
      <td id="balanceSort">
        Balance
      </td>
      <td>
        Registered
      </td>
      <td>
        Employers
      </td>
    </tr>
  `;
  table.innerHTML = createFirstTr;

  //********************* Сортировка по названию компаний*********************//
  companySort.addEventListener('click', newArrayCompany);

  function newArrayCompany() { ///// Сортировка масива компании в алфавитном порядке
    let sortArrayCompany = data.sort(function(a, b) {
      return a.company > b.company;
    });

    sortArrayCompany.map(function(item) { ///// Рендер таблицы
      let delTable = document.getElementById('tabletr');
      delTable.remove();
      let infoTable = new Info(item);
    });
  };

  //********************* Сортировка баланса от меньшего к большему*********************//
  balanceSort.addEventListener('click', newArrayBalance);

  let newBalance = data.map((item, i) => { ///// удаление знака $ с массива
    let beginBalance = item.balance.split('');
    beginBalance.shift();
    let number = beginBalance.join('');
    item.balance = number;
    return item;
  });


  function newArrayBalance() {
    let sortArrayBalance = data.sort(function(a, b) { ///// Сортировка масива по balance
      return a.balance - b.balance;
    });
    sortArrayBalance.map(function(item) { ///// Рендер таблицы
      let delTable = document.getElementById('tabletr');
      delTable.remove();
      let infoTable = new Info(item)
    });
  };

  //********************* Наполнение основной таблицы и таблицы 2*********************//
  function Info(item) {
    let {
      company,
      balance,
      address,
      employers,
      _id
    } = item;
    let {
      city,
      zip,
      country,
      state,
      street,
      house
    } = item.address;
    render();

    //**Отображение адреса в отдельном окне**//
    function showAddres(event) { //отображение адреса с отдельном окне
      let buttonShowAddres = event.target.dataset.id;

      if (buttonShowAddres === item._id) {
        let boxView = document.getElementById('viewAddres');
        boxView.classList.add("popup_adress");

        let divShow = document.createElement('div')
        boxView.appendChild(divShow);
        divShow.classList.add('popup');
        divShow.setAttribute('id', 'divShow');

        let pTag = document.createElement('p');
        divShow.appendChild(pTag);
        pTag.innerText = `${company}: ${city}; ${zip}; ${country}; ${state}; ${street}; ${house}`;
      };

      function removeShowAddres() { //удаление отдельного окна с адресом
        let divShow = document.getElementById('divShow');
        viewAddres.removeChild(divShow);
        viewAddres.classList.remove("popup_adress");
      };
      viewAddres.addEventListener('click', removeShowAddres);
    };

    //**Отображение данных о сотрудниках в отдельной таблице**//
    function showEmployes(event) {
      let buttonShowEmployes = event.target.dataset.id;

      if (buttonShowEmployes === item._id) { //создание таблици2 как отдельного обьекта
        firstTrTableTwo();
        item.employers.map((item) => {
          renderTableTwo(item);
        });
        arrEmpl = item.employers;

        //**********Сортировка сотрудников по именам*********//
        nameEmpl.addEventListener('click', newArrayName);

        function newArrayName() { ///// Сортировка имен в алфавитном порядке
          let sortArrayName = arrEmpl.sort(function(a, b) {
            return a.name > b.name;
          });
          sortArrayName.map(function(item) { ///// Рендер таблицы
            let delTableTwo = document.getElementById('tableTwoDel');
            delTableTwo.remove();
            renderTableTwo(item);
          });
        };
        //**********Сортировка сотрудников по возрасту*********//
        age.addEventListener('click', newArrayAge);

        function newArrayAge() { ///// Сортировка имен в алфавитном порядке
          let sortArrayAge = arrEmpl.sort(function(a, b) {
            return a.age - b.age;
          });
          sortArrayAge.map(function(item) { ///// Рендер таблицы
            let delTableTwo = document.getElementById('tableTwoDel');
            delTableTwo.remove();
            renderTableTwo(item);
          });
        };
        //**********Сортировка сотрудников по полу*********//
        gender.addEventListener('click', newArrayGender);

        function newArrayGender() { ///// Сортировка по возрасту
          let sortArrayGender = arrEmpl.sort(function(a, b) {
            return a.gender > b.gender;
          });
          sortArrayGender.map(function(item) { ///// Рендер таблицы
            let delTableTwo = document.getElementById('tableTwoDel');
            delTableTwo.remove();
            renderTableTwo(item);
          });
        };

      };


      function firstTrTableTwo() { //создание таблицы2 - заголовок + шапка
        let boxView = document.getElementById('viewEmpoyes');
        viewEmpoyes.classList.add("popup_employes");

        let divTable2 = document.createElement('div');
        boxView.appendChild(divTable2);
        divTable2.setAttribute('id', 'divTable2');

        let headName = document.createElement('h1');
        divTable2.appendChild(headName);
        headName.innerText = "Employers of company: " + company;

        let buttonBack = document.createElement('button');
        divTable2.appendChild(buttonBack);
        buttonBack.setAttribute('id', 'button');
        buttonBack.classList.add('button_color');
        buttonBack.innerText = 'BACK';
        button.addEventListener('click', removeTable2);

        let tableTag = document.createElement('table');
        tableTag.setAttribute('id', 'tableTwo');
        tableTag.setAttribute('border', '1px solid black');
        tableTag.classList.add('tableTwo');
        divTable2.appendChild(tableTag);

        let tableFirstTr = `
          <tr>
            <td id="nameEmpl">
              Name
            </td>
            <td id="gender">
              Gender
            </td>
            <td id="age">
              Age
            </td>
            <td>
              Contacts
            </td>
          </tr>
        `;
        tableTag.innerHTML = tableFirstTr;

      };

      function renderTableTwo(empl) { //наполнение талицы2 данными о сутрудниках
        let {
          name,
          phones,
          age,
          emails,
          gender,
          id
        } = empl;

        let otherTrTableTwo = document.createElement('tr');
        let tableTwo = document.getElementById('tableTwo')
        otherTrTableTwo.setAttribute('id', 'tableTwoDel');
        tableTwo.appendChild(otherTrTableTwo);

        let tdName = document.createElement('td');
        otherTrTableTwo.appendChild(tdName);
        tdName.innerText = name;
        console.log(name);

        let tdGender = document.createElement('td');
        otherTrTableTwo.appendChild(tdGender);
        tdGender.innerText = gender;

        let tdAge = document.createElement('td');
        otherTrTableTwo.appendChild(tdAge);
        tdAge.innerText = age;

        let tdPhone = document.createElement('td');
        otherTrTableTwo.appendChild(tdPhone);
        tdPhone.innerText = phones;
      };

      function removeTable2() { //удаление таблици 2
        let boxView = document.getElementById('viewEmpoyes');
        viewEmpoyes.classList.remove("popup_employes");
        viewEmpoyes.removeChild(divTable2);
      };
    };

    //********************* Создание строк основной таблицы, их наполнение + обработчики на кнопки//*********************

    function render() {
      let otherTr = document.createElement('tr');
      otherTr.setAttribute('id', 'tabletr');
      table.appendChild(otherTr);

      let tdCompany = document.createElement('td');
      otherTr.appendChild(tdCompany);
      tdCompany.innerHTML = company;

      let tdBalance = document.createElement('td');
      otherTr.appendChild(tdBalance);
      tdBalance.innerHTML = balance;

      let tdRegistered = document.createElement('td');
      otherTr.appendChild(tdRegistered);
      let buttonViewAddre = document.createElement('button');
      tdRegistered.appendChild(buttonViewAddre);
      buttonViewAddre.classList.add('button_color');
      buttonViewAddre.innerText = 'Показать адресс';
      buttonViewAddre.dataset.id = _id;
      buttonViewAddre.addEventListener('click', showAddres); //обработчик на показ адреса


      let tdEmployers = document.createElement('td');
      otherTr.appendChild(tdEmployers);
      let buttonViewEmploy = document.createElement('button');
      tdEmployers.appendChild(buttonViewEmploy);
      buttonViewEmploy.classList.add('button_color');
      buttonViewEmploy.innerText = 'Показать сотрудников компании';
      buttonViewEmploy.dataset.id = _id;
      buttonViewEmploy.addEventListener('click', showEmployes); //обработчик на показ сотрудников

    };
  };
  //*********************Cоздание основной таблицы как объекта//*********************
  let myTable = data.map((item) => {
    let infoTable = new Info(item);
  });
};
